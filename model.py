import numpy as np
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
# Tensorflow
import tensorflow as tf
# Numpy and Pandas
import pandas as pd
# Ohter import
import sys
from tensorflow.keras.models import Sequential
from tensorflow.keras import layers


images = np.load('images.npy')
labels = np.load('labels.npy')

images_train , images_test , labels_train , labels_test = train_test_split(images,labels,test_size=0.1,random_state=1)

model = Sequential([
  # data augmentation
  layers.RandomFlip("horizontal",
                     input_shape=(180,
                                  180,
                                  3)),
  layers.RandomRotation(0.1),
  layers.RandomZoom(0.1),
  # model architecture
  layers.Rescaling(1./255),
  layers.Conv2D(16, 3, padding='same', activation='relu'),
  layers.MaxPooling2D(),
  layers.Conv2D(32, 3, padding='same', activation='relu'),
  layers.MaxPooling2D(),
  layers.Conv2D(64, 3, padding='same', activation='relu'),
  layers.MaxPooling2D(),
  layers.Dropout(0.2),
  layers.Flatten(),
  layers.Dense(128, activation='relu'),
  layers.Dense(5)
])


# and compiled with
model.compile(
    optimizer='adam',
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=['accuracy']
    )

history = model.fit(images_train, labels_train, epochs=10, validation_split=0.2)
model.save("model.h5")

def display_metrix():
    loss_curve = history.history["loss"]
    acc_curve = history.history["accuracy"]

    loss_val_curve = history.history["val_loss"]
    acc_val_curve = history.history["val_accuracy"]

    plt.plot(loss_curve, label="Train")
    plt.plot(loss_val_curve, label="Val")
    plt.legend(loc='upper left')
    plt.title("Loss")
    plt.show()

    plt.plot(acc_curve, label="Train")
    plt.plot(acc_val_curve, label="Val")
    plt.legend(loc='upper left')
    plt.title("Accuracy")
    plt.show()

def evaluate_test():
    loss, acc = model.evaluate(images_test, labels_test)
    print("Test Loss", loss)
    print("Test Accuracy", acc)

display_metrix()
evaluate_test()