import numpy as np
import os
import cv2
import random

def readImages (folder) :
    """
    :param folder: folder which contain images to train/validate/test the model
    :return: images to train/validate/test
    """
    images = []
    files = os.listdir(folder)
    for file in files :
        image = cv2.imread(folder+'/'+file)
        image = cv2.resize(image,(180,180))
        images.append(image)
    #print(f'{images.shape} images')
    print('done')
    return images

def putlabels(n,labels,k):
    """
    :param n: length of one group of images / example : length of sunflowers
    :param labels: list of labels, it will append the same number k
    :param k: label of the group / example : k=3 for sunflowers
    """
    for i in range(0, n) :
        labels.append(k)



def shuffle (images,labels):
    """
    :param images: images to shuffle
    :param labels: labels to shuffle
    :return: images and labels shuffled
    The objective here is to shuffle the index (idx) of these to lists so it can shuffle with the same order
    """
    idx = []
    x = []
    y = []
    for i in range(0,len(images)) :
        idx.append(i)
    random.shuffle(idx)
    for i in range(0,len(images)) :
        x.append(images[idx[i]])
        y.append(labels[idx[i]])
    return x,y

#################    loading data    ##################
path = 'flower_photos/'
labels= []
images= []
daisy = readImages(path + 'daisy')
putlabels(len(daisy),labels,0)
dandelion = readImages(path + 'dandelion')
putlabels(len(dandelion),labels,1)
roses = readImages(path + 'roses')
putlabels(len(roses),labels,2)
sunflowers = readImages(path + 'sunflowers')
putlabels(len(sunflowers),labels,3)
tulips = readImages(path + 'tulips')
putlabels(len(tulips),labels,4)
images = daisy + dandelion + roses + sunflowers + tulips

"""
    label : group of images
    0 : daisy
    1 : dandelion
    2 : roses 
    3 : sunflowers
    4 : tulips
    """


images_shuffle,labels_shuffle = shuffle(images,labels)
################ transform into an array ############
images_array = np.array(images_shuffle,dtype=np.float32)
labels_array = np.array(labels_shuffle)
########### save arrays and load them to model.py ############
np.save('images.npy',images_array)
np.save('labels.npy',labels_array)